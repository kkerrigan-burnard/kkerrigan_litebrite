﻿/*
 * Kristian Kerrigan
 * Tile.cs
 * This class is the model used for the Lite Brite tiles on the board.
 */

using System;

namespace KKerrigan_LiteBrite.Models
{
    [Serializable]
    public class Tile
    {
        private string color;

        public string Color
        {
            get { return color; }
            set
            {
                color = value;

            }
        }


        // Constructors
        // No -arg constructor needed for Serialization 
        public Tile() { }

        public Tile(string color)
        {
            Color = color;
        }


    }
}
