﻿/*
 * Kristian Kerrigan
 * ColorProperty.cs
 * This class is the model used for the Lite Brite pegs, and contains the Name of the color as a string
 * and the actual Color object (to be displayed in the list box)
 */

using System.Windows.Media;

namespace KKerrigan_LiteBrite.Models
{
    public class ColorProperty
    {
        public string Name { get; set; }
        public Color Color { get; set; }

        public override string ToString()
        {
            return Name;
        }
    }
}
