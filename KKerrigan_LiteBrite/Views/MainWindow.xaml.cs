﻿/*
 * Kristian Kerrigan
 * MainWindow.xaml.cs
 * This file contains the code behind for the MainWindow.xaml
 */

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;

using KKerrigan_LiteBrite.View_Models;

namespace KKerrigan_LiteBrite
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Point startPoint;

        public MainWindow()
        {
            InitializeComponent();
            DataContext = new LiteBriteViewModel();
        }

        private void ApplicationClose(object sender, ExecutedRoutedEventArgs e)
        {
            MessageBoxResult prompt = MessageBox.Show("Are you sure you wish to close Lite Brite, all unsaved changes will be lost?", 
                "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (prompt.Equals(MessageBoxResult.Yes))
                Close();
        }

        /*
  * Helper method to search up the VisualTree to identify whether the user 
  * clicked "on" the right type of control. If the control that fired the event
  * isn't the right type then perhaps it's a descendent of another control that 
  * is the right type. The parameter 'current' is any visual component which 
  * extends DependencyObject. The generic argument T is the type of visual component
  * that we're looking for within the VisualTree of our GUI, such as ListBoxItem. 
  * If the specified type of visual component is found within the VisualTree 
  * then an object reference of that type is returned, otherwise null is returned.
  * From: http://wpftutorial.net/DragAndDrop.html
  */
        private static T findAncestor<T>(DependencyObject current)
            where T : DependencyObject
        {
            do
            {
                if (current is T)
                {
                    return (T)current;
                }
                current = VisualTreeHelper.GetParent(current);
            }
            while (current != null);
            return null;
        }

        private void lbColorTiles_PreviewMouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            startPoint = e.GetPosition(null);  // absolute position
        }

        private void lbColorTiles_MouseMove(object sender, MouseEventArgs e)
        {
            // step 1: - detect a drag and drop operation
            // Get the mouse position and difference since starting the drag
            Point mousePos = e.GetPosition(null);
            Vector diff = startPoint - mousePos;
            if (e.LeftButton == MouseButtonState.Pressed &&
                (Math.Abs(diff.X) > SystemParameters.MinimumHorizontalDragDistance ||
                Math.Abs(diff.Y) > SystemParameters.MinimumVerticalDragDistance))
            {
                // get the dragged listbox item
                ListBox listBox = (ListBox)sender;
                ListBoxItem listBoxItem = findAncestor<ListBoxItem>((DependencyObject)e.OriginalSource);
                if (listBoxItem != null)
                {
                    // Find the data behind the listBoxItem
                    string theItem = listBox.ItemContainerGenerator.ItemFromContainer(listBoxItem).ToString();
                    // initialize drag and drop
                    // Step 2: create a DataObject containing the string to be "dragged"
                    DataObject dragData = new DataObject(typeof(string), theItem);
                    // Step 3: initialize the dragging
                    DragDrop.DoDragDrop(listBoxItem, dragData, DragDropEffects.Move);
                }
            }
        }

        private void liteBrite_DragEnter(object sender, DragEventArgs e)
        {
            if (!e.Data.GetDataPresent(typeof(string)) || sender == e.Source)
            {
                e.Effects = DragDropEffects.None;
            }
        }

        private void liteBrite_Drop(object sender, DragEventArgs e)
        {
            if (e.Data.GetDataPresent(typeof(string)))
            {
                String theItem = e.Data.GetData(typeof(string)).ToString();
                

                var brushConverter = new BrushConverter();
                var color = brushConverter.ConvertFromString(theItem);

                ((Button )sender).Background = (Brush)color;

            }
        }
    }
}
