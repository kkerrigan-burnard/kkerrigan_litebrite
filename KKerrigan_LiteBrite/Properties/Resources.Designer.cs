﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace KKerrigan_LiteBrite.Properties {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("KKerrigan_LiteBrite.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _About.
        /// </summary>
        public static string AboutHeader {
            get {
                return ResourceManager.GetString("AboutHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to About Lite Brite.
        /// </summary>
        public static string AboutToolTip {
            get {
                return ResourceManager.GetString("AboutToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset Current Lite Brite Image.
        /// </summary>
        public static string DeleteToolTip {
            get {
                return ResourceManager.GetString("DeleteToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Exit.
        /// </summary>
        public static string ExitHeader {
            get {
                return ResourceManager.GetString("ExitHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _File.
        /// </summary>
        public static string FileHeader {
            get {
                return ResourceManager.GetString("FileHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Game.
        /// </summary>
        public static string GameHeader {
            get {
                return ResourceManager.GetString("GameHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Help.
        /// </summary>
        public static string HelpHeader {
            get {
                return ResourceManager.GetString("HelpHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _New.
        /// </summary>
        public static string NewHeader {
            get {
                return ResourceManager.GetString("NewHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open Existing Lite Brite Image.
        /// </summary>
        public static string OpenFileToolTipe {
            get {
                return ResourceManager.GetString("OpenFileToolTipe", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Open.
        /// </summary>
        public static string OpenHeader {
            get {
                return ResourceManager.GetString("OpenHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Save Current Lite Brite Image.
        /// </summary>
        public static string SaveFileToolTip {
            get {
                return ResourceManager.GetString("SaveFileToolTip", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to _Save.
        /// </summary>
        public static string SaveHeader {
            get {
                return ResourceManager.GetString("SaveHeader", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Lite Brite - Kristian Kerrigan © 2018.
        /// </summary>
        public static string WindowTitle {
            get {
                return ResourceManager.GetString("WindowTitle", resourceCulture);
            }
        }
    }
}
