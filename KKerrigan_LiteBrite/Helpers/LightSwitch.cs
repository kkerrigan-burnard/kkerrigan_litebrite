﻿/*
 * Kristian Kerrigan
 * LightSwitch.cs
 * This class is a converter used to change the tile color's opacity value to simulate turning 
 * on the Lite Brite.
 */

using System;
using System.Globalization;
using System.Windows.Data;

namespace KKerrigan_LiteBrite.Helpers
{
    public class LightSwitch : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool isLightsOn = false;
            Boolean.TryParse(value.ToString(), out isLightsOn);

            double opacity = 1;
            if (!isLightsOn)
            {
                opacity = 0.3;
            }

            return opacity;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
