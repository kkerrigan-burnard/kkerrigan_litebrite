﻿/*
 * Kristian Kerrigan
 * LiteBriteViewModel.cs
 * This class handles all the view model logic, such as binding, so that the view
 * and the models can communicate with one another.
 */

using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows;

using System.Windows.Media;
using System.Reflection;
using Microsoft.Win32;
using System.Xml.Serialization;
using System.IO;

using KKerrigan_LiteBrite.Models;
using KKerrigan_LiteBrite.Helpers;

namespace KKerrigan_LiteBrite.View_Models
{
    public class LiteBriteViewModel : DependencyObject
    {
        // Dependency Objects
        public ObservableCollection<Tile> Tiles
        {
            get { return (ObservableCollection<Tile>)GetValue(TilesProperty); }
            set { SetValue(TilesProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Tiles.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TilesProperty =
            DependencyProperty.Register("Tiles", typeof(ObservableCollection<Tile>), typeof(LiteBriteViewModel), new PropertyMetadata(null));



        public ObservableCollection<ColorProperty> Colors
        {
            get { return (ObservableCollection<ColorProperty>)GetValue(ColorsProperty); }
            set { SetValue(ColorsProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Colors.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ColorsProperty =
            DependencyProperty.Register("Colors", typeof(ObservableCollection<ColorProperty>), typeof(LiteBriteViewModel), new PropertyMetadata(null));

        // Relay Commands
        public RelayCommand OpenFileCommand { get; set; }
        public RelayCommand SaveFileCommand { get; set; }
        public RelayCommand NewLiteBriteCommand { get; set; }
        public RelayCommand AboutCommand { get; set; }

        // Constructor
        public LiteBriteViewModel()
        {
            Colors = new ObservableCollection<ColorProperty>();
            GenerateColors();
            Tiles = new ObservableCollection<Tile>();

            // Generate empty tiles
            for (int i = 0; i < 2500; ++i)
            {
                Tiles.Add(new Tile("Black"));
            }

            // Set Relay Commands
            OpenFileCommand = new RelayCommand(OpenLiteBriteImage);
            SaveFileCommand = new RelayCommand(SaveLiteBriteImage);
            NewLiteBriteCommand = new RelayCommand(ClearLiteBrite);
            AboutCommand = new RelayCommand(AboutLiteBrite);
        }

        private void GenerateColors()
        {
            // Use Reflection to obtain all named colors
            foreach (var color in typeof(Colors).GetRuntimeProperties())
            {
                Colors.Add(new ColorProperty() { Name = color.Name, Color = (Color)color.GetValue(null) });
            }

            // Remove duplicate / unwanted colors
            string[] unwantedColors =
            {
                "Dark", "Light", "Medium", "AliceBlue", "Azure",
                "Cornsilk", "AntiqueWhite", "Deep", "Transparent", "SeaShell", "Snow",
                "Pale", "PapayaWhip", "PeachPuff", "OldLace", "Olive", "Beige", "Bisque",
                "BurlyWood", "DimGray", "FloralWhite", "Gainsboro", "GhostWhite", "Honeydew",
                "Ivory", "Lavender", "Lemon", "Linen", "Misty", "Mint", "Moccasin", "NavajoWhite",
                "Peru", "Pink", "Plum", "Powder", "Rosy", "Saddle", "Sienna", "Silver",
                "Steel", "Slate", "Tan", "Thistle", "Wheat", "Lime", "Lawn", "GreenYellow",
                "Goldenrod", "YellowGreen", "Gray", "Forest"
            };

            foreach (string searchTerm in unwantedColors)
            {
                foreach (var color in Colors.Where(c => c.Name.StartsWith(searchTerm)).ToList())
                {
                    Colors.Remove(color);
                }
            }
        }

        void SaveLiteBriteImage(object paramter)
        {
            SaveFileDialog dialog = new SaveFileDialog();
            dialog.DefaultExt = ".brite";
            dialog.Filter = "LiteBrite (*.brite)|*.brite";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            bool? file = dialog.ShowDialog();

            if (file == true)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Tile>));
                    using (StreamWriter writer = new StreamWriter(dialog.OpenFile()))
                    {
                        serializer.Serialize(writer, Tiles);
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("The contents could not be saved to a file:\n" + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                }
            }
        }

        void OpenLiteBriteImage(object parameter)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.DefaultExt = ".brite";
            dialog.Filter = "LiteBrite (*.brite)|*.brite";
            dialog.InitialDirectory = AppDomain.CurrentDomain.BaseDirectory;

            bool? file = dialog.ShowDialog();

            if (file == true)
            {
                try
                {
                    XmlSerializer serializer = new XmlSerializer(typeof(ObservableCollection<Tile>));
                    using (StreamReader reader = new StreamReader(dialog.OpenFile()))
                    {
                        Tiles = serializer.Deserialize(reader) as ObservableCollection<Tile>;
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show("The selected file could not be oppened:\n" + ex.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
                    Tiles = new ObservableCollection<Tile>();
                }
            }
        }

        void ClearLiteBrite(object parameter)
        {
            MessageBoxResult prompt = MessageBox.Show("Are you sure you wish to clear the Lite Brite, all unsaved changes will be lost?", 
                "Are you sure?", MessageBoxButton.YesNo, MessageBoxImage.Question);

            if (prompt.Equals(MessageBoxResult.Yes))
            {
                // Set the Tiles dp to new Observable Collection and regenerate the tiles
                Tiles = new ObservableCollection<Tile>();

                for (int i = 0; i < 2500; ++i)
                {
                    Tiles.Add(new Tile("Black"));
                }
            }
        }

        void AboutLiteBrite(object parameter)
        {
            MessageBox.Show("Lite Brite Simulator\nKristian Kerrigan © 2018", "About", MessageBoxButton.OK, MessageBoxImage.Information);
        }
    }
}
